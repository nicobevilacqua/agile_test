import axios from 'axios'

const apiKey = '23567b218376f79d9415'
const origin = 'http://interview.agileengine.com'

function getAccessToken() {
  return localStorage.getItem('accessToken')
}

function setAccesToken(token) {
  localStorage.setItem('accessToken', token)
}

axios.interceptors.request.use(
  (config) => {
    const token = getAccessToken()
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

axios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    const request = error.config
    const {
      response: { status },
    } = error
    if (status === 401 && request.url === `${origin}/auth`) {
      return Promise.reject(error)
    }
    if (status === 401 && !request._retry) {
      request._retry = true
      return axios
        .post(`${origin}/auth`, { apiKey })
        .then(({ status, data }) => {
          if (status === 200) {
            const { token } = data
            setAccesToken(token)
            axios.defaults.headers.common.Authorization = `Bearer ${token}`
            return axios(request)
          }
        })
    }
    return Promise.reject(error)
  }
)

export async function getPage(page = 1) {
  const { data } = await axios.get(`${origin}/images?page=${page}`)
  return data
}

export async function getImage(id) {
  const { data } = await axios.get(`${origin}/images/${id}`)
  return data
}
